package controller;

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

import data.Client;
import data.QuickSortArrayListClient;
import data.SimulationQueue;
import frontEnd.SimulationDisplay;

public class Controller extends Thread {

	public String info;

	private SimulationDisplay simulationDisplay;

	private StringBuilder logText = new StringBuilder();

	private final int ONE_TIME_UNIT = 1000;
	private Random rand = new Random();
	private int simulationTime = 0;
	private ArrayList<Client> clients = null;
	private int currentClientIndex = 0;
	private int nrClients = 0;
	private final int nrQueues;

	private PriorityQueue<SimulationQueue> queues = new PriorityQueue<SimulationQueue>();

	public Controller(int nrClients, int nrQueues, int simulationTime, int minArrivalTime, int maxArrivalTime,
			int minServiceTime, int maxServiceTime) {

		this.simulationTime = simulationTime;
		this.nrClients = nrClients;
		this.nrQueues = nrQueues;

		for (int i = 0; i < nrQueues; i++) {
			queues.add(new SimulationQueue(simulationTime, i));
		}

		this.clients = generateClients(nrClients, minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime);

		simulationDisplay = new SimulationDisplay(nrQueues, this);

		start();
	}

	public void run() {

		int currentTime = -1;

		while (currentTime < simulationTime) {

			currentTime++;

			for (int i = this.currentClientIndex; i < this.nrClients; i++) {

				if (clients.get(i).getArrivalTime() == currentTime) {

					SimulationQueue tempQueue = queues.poll();

					tempQueue.addClient(clients.get(i));

					queues.add(tempQueue);

					this.currentClientIndex++;
				} else {

					break;
				}
			}

			try {

				info = loadStatus(currentTime);

			} catch (Exception ex) {
				ex.printStackTrace();
			}

			try {
				sleep(ONE_TIME_UNIT);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

		loadQueuesData();

		File file = new File("log.txt");

		if (file.delete()) {
			// success
		} else {
			// failure
		}

		file = new File("log.txt");

		try {
			if (file.createNewFile()) {
				System.out.println("File created: " + file.getName());
			} else {
				System.out.println("File already exists.");
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

		try {
			FileWriter writer = new FileWriter("log.txt");
			writer.write(logText.toString());
			writer.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	private ArrayList<Client> generateClients(int nrClients, int minArrivalTime, int maxArrivalTime, int minServiceTime,
			int maxServiceTime) {

		ArrayList<Client> generatedClients = new ArrayList<Client>();

		for (int i = 0; i < nrClients; i++) {

			int clientArrivalTime = rand.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
			int clientServiceTime = rand.nextInt(maxServiceTime - minServiceTime) + minServiceTime;

			generatedClients.add(new Client(i, clientArrivalTime, clientServiceTime));
		}

		QuickSortArrayListClient sortingObject = new QuickSortArrayListClient(generatedClients);

		return sortingObject.plsSort();
	}

	private String loadStatus(int currentTime) {

		StringBuilder tempSB = new StringBuilder();

		tempSB.append("Time ");
		tempSB.append(currentTime);
		tempSB.append("\n");

		logText.append("Time ");
		logText.append(currentTime);
		logText.append("\n");

		for (int i = this.currentClientIndex; i < this.nrClients; i++) {

			tempSB.append("(");
			tempSB.append(this.clients.get(i).getClientID());
			tempSB.append(",");
			tempSB.append((this.clients.get(i).getArrivalTime() + 1));
			tempSB.append(",");
			tempSB.append(this.clients.get(i).getServiceTime());
			tempSB.append(")\n");

			logText.append("(");
			logText.append(this.clients.get(i).getClientID());
			logText.append(",");
			logText.append((this.clients.get(i).getArrivalTime() + 1));
			logText.append(",");
			logText.append(this.clients.get(i).getServiceTime());
			logText.append(")\n");
		}

		Iterator<SimulationQueue> it = queues.iterator();
		StringBuilder[] queuesString = new StringBuilder[this.nrQueues];

		while (it.hasNext()) {

			SimulationQueue currentQueue = it.next();
			Queue<Client> currentQueueClients = new LinkedList(currentQueue.getClients());

			int qID = currentQueue.getQueueID();
			queuesString[qID] = new StringBuilder("");
			queuesString[qID] = new StringBuilder("");
			queuesString[qID].append("Queue ");
			queuesString[qID].append(currentQueue.getQueueID());
			queuesString[qID].append(" ");

			for (Client currentClient : currentQueueClients) {

				if (currentClient.getServiceTime() == 0) {
					continue;
				}

				queuesString[qID].append("(");
				queuesString[qID].append(currentClient.getClientID());
				queuesString[qID].append(" , ");
				queuesString[qID].append((currentClient.getArrivalTime() + 1));
				queuesString[qID].append(" , ");
				queuesString[qID].append(currentClient.getServiceTime());
				queuesString[qID].append("), ");
			}

			if (currentQueueClients.isEmpty()) {
				// logText.append("closed");
				queuesString[qID].append("closed");
			}
			// logText.append("\n");
			queuesString[qID].append("\n");
		}

		for (int i = 0; i < this.nrQueues; i++) {
			tempSB.append(queuesString[i]);
			logText.append(queuesString[i].toString());
		}

		tempSB.append("\n\n");
		logText.append("\n\n");

		return tempSB.toString();
	}

	private void loadQueuesData() {

		double avgServiceTime = 0;
		double avgWaitingTime = 0;
		double avgPeakHour = 0;

		for (SimulationQueue currentQueue : this.queues) {

			avgServiceTime += currentQueue.computeAverageServiceTime();
			avgWaitingTime += currentQueue.computeAverageWaitingTime();
			avgPeakHour += currentQueue.getPeakHour();

			logText.append("Queue ");
			logText.append(currentQueue.getQueueID());
			logText.append("  Average Waiting Time -> ");
			logText.append(currentQueue.computeAverageWaitingTime());
			logText.append("  Average Service Time -> ");
			logText.append(currentQueue.computeAverageServiceTime());
			logText.append("  Peak Hour -> ");
			logText.append(currentQueue.getPeakHour());
			logText.append("\n\n");
		}

		avgServiceTime /= this.nrQueues;
		avgWaitingTime /= this.nrQueues;
		avgPeakHour /= this.nrQueues;

		logText.append("In total: ");
		logText.append("  Average Waiting Time -> ");
		logText.append(avgWaitingTime);
		logText.append("  Average Service Time -> ");
		logText.append(avgServiceTime);
		logText.append("  Peak Hour -> ");
		logText.append(avgPeakHour);
		logText.append("\n\n");
	}
}
