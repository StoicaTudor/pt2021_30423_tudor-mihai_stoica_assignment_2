package data;

import java.util.LinkedList;
import java.util.Queue;

public class SimulationQueue extends Thread implements Comparable<SimulationQueue> {

	private final int ONE_TIME_UNIT = 1000;
	private Queue<Client> clients = new LinkedList<Client>();
	private final int simulationTime;
	private int queueID = 0;

	private int currentTotalWaitingTime = 0;

	private int totalWaitingTime = 0;
	private int totalNrClientsAllTime = 0;
	private int totalServiceTime = 0;

	private int peakHour = 0;

	public SimulationQueue(int simulationTime, int queueID) {

		this.queueID = queueID;
		this.simulationTime = simulationTime;
		start();
	}

	public void addClient(Client newClient) {

		clients.add(newClient);
		totalNrClientsAllTime++;
		currentTotalWaitingTime += newClient.getServiceTime();
	}

	public void run() {

		int currentTime = -1;

		while (currentTime < simulationTime) {

			try {
				sleep(ONE_TIME_UNIT);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			currentTime++;

			if (!clients.isEmpty()) {

				currentTotalWaitingTime--;

				clients.forEach((currentClient) -> {

					currentClient.timeUnitPassed(); // maybe not necessary
				});

				totalWaitingTime += clients.size() - 1; // add every client's waiting time (1 time unit), except for
														// the front client

				if (clients.size() > peakHour) {
					peakHour = clients.size();
				}

				if (clients.peek().decrementServiceTime()) {

					totalServiceTime += clients.peek().getOriginalServiceTime();
					clients.remove();
				}
			}
		}
	}

	public double computeAverageServiceTime() {

		return (double) (this.totalServiceTime / this.totalNrClientsAllTime);
	}

	public double computeAverageWaitingTime() {
		return (double) (this.totalWaitingTime / this.totalNrClientsAllTime);
	}

	public int getPeakHour() {
		return this.peakHour;
	}

	public int getCurrentTotalWaitingTime() {
		return this.currentTotalWaitingTime;
	}

	@Override
	public int compareTo(SimulationQueue secondQueue) {

		if (this.currentTotalWaitingTime > secondQueue.getCurrentTotalWaitingTime()) {
			return 1;
		} else if (this.currentTotalWaitingTime < secondQueue.getCurrentTotalWaitingTime()) {
			return -1;
		} else {
			return 0;
		}
	}

	public Queue<Client> getClients() {
		return this.clients;
	}

	public int getCurrentNrClientsInQueue() {
		return this.clients.size();
	}

	public int getQueueID() {
		return this.queueID;
	}
}
