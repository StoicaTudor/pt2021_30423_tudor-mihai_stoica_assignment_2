package data;

import java.util.ArrayList;
import java.util.Collections;

public class QuickSortArrayListClient{
	
	private ArrayList<Client> arrayToSort = null;
	
	public ArrayList<Client> getSortedArray() {
		return arrayToSort;
	}
	
	public QuickSortArrayListClient(ArrayList<Client> arrayToSort) {
		
		this.arrayToSort = arrayToSort;
	}
	
	public ArrayList<Client> plsSort() {
		
		quickSort(0, arrayToSort.size() - 1);
		return arrayToSort;
	}
	
	private void quickSort(int left, int right) {
		
		if(left < right) {
			
			int partition = doPartition(left, right);
			quickSort(left, partition - 1);
			quickSort(partition + 1, right);
		}
	}
	
	private int doPartition(int left, int right) {
		
		int pivot = arrayToSort.get(right).getArrivalTime();
		
		int indexSmallerElement = left - 1;
		
		for(int j = left ; j <= right - 1 ; j++) {
			
			if(arrayToSort.get(j).getArrivalTime() < pivot) {
				
				indexSmallerElement++;
				Collections.swap(arrayToSort, indexSmallerElement, j);
			}
		}
		Collections.swap(arrayToSort, indexSmallerElement + 1, right);
		return indexSmallerElement + 1;
	}

}
