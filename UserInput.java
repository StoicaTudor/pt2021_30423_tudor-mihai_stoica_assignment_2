package frontEnd;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import controller.Controller;

public class UserInput {

	private JFrame frame;
	private JTextField inputClients;
	private JTextField inputQueues;
	private JTextField inputSimulationTime;
	private JTextField inputMinArrivalTime;
	private JTextField inputMaxArrivalTime;
	private JTextField inputMinServiceTime;
	private JTextField inputMaxServiceTime;
	private JTextField txtClients;
	private JTextField txtQueues;
	private JTextField txtSimulationTime;
	private JTextField txtArrivalTime;
	private JTextField txtServiceTime;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserInput window = new UserInput();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UserInput() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 900, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		inputClients = new JTextField();
		inputClients.setBounds(226, 162, 116, 22);
		frame.getContentPane().add(inputClients);
		inputClients.setColumns(10);

		inputQueues = new JTextField();
		inputQueues.setBounds(226, 250, 116, 22);
		frame.getContentPane().add(inputQueues);
		inputQueues.setColumns(10);

		inputSimulationTime = new JTextField();
		inputSimulationTime.setBounds(226, 349, 116, 22);
		frame.getContentPane().add(inputSimulationTime);
		inputSimulationTime.setColumns(10);

		inputMinArrivalTime = new JTextField();
		inputMinArrivalTime.setBounds(226, 456, 116, 22);
		frame.getContentPane().add(inputMinArrivalTime);
		inputMinArrivalTime.setColumns(10);

		inputMaxArrivalTime = new JTextField();
		inputMaxArrivalTime.setBounds(545, 456, 116, 22);
		frame.getContentPane().add(inputMaxArrivalTime);
		inputMaxArrivalTime.setColumns(10);

		inputMinServiceTime = new JTextField();
		inputMinServiceTime.setBounds(226, 550, 116, 22);
		frame.getContentPane().add(inputMinServiceTime);
		inputMinServiceTime.setColumns(10);

		inputMaxServiceTime = new JTextField();
		inputMaxServiceTime.setBounds(545, 550, 116, 22);
		frame.getContentPane().add(inputMaxServiceTime);
		inputMaxServiceTime.setColumns(10);

		JButton startSimButton = new JButton("Start Simulation");
		startSimButton.setBounds(369, 655, 125, 25);

		startSimButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int nrClients = Integer.parseInt(inputClients.getText());
				int nrQueues = Integer.parseInt(inputQueues.getText());
				int simulationTime = Integer.parseInt(inputSimulationTime.getText());
				int minArrivalTime = Integer.parseInt(inputMinArrivalTime.getText());
				int maxArrivalTime = Integer.parseInt(inputMaxArrivalTime.getText());
				int minServiceTime = Integer.parseInt(inputMinServiceTime.getText());
				int maxServiceTime = Integer.parseInt(inputMaxServiceTime.getText());

				if (minArrivalTime > maxArrivalTime || minServiceTime > maxServiceTime || nrClients < 0 || nrQueues < 0
						|| simulationTime < 0 || minArrivalTime < 0 || maxArrivalTime < 0 || minServiceTime < 0
						|| maxServiceTime < 0) {
					JOptionPane.showMessageDialog(null, "input not correct");
					return;
				}

				new Controller(nrClients, nrQueues, simulationTime, minArrivalTime, maxArrivalTime, minServiceTime,
						maxServiceTime);
			}
		});
		frame.getContentPane().add(startSimButton);

		txtClients = new JTextField();
		txtClients.setHorizontalAlignment(SwingConstants.CENTER);
		txtClients.setText("Clients");
		txtClients.setEditable(false);
		txtClients.setBounds(61, 162, 116, 22);
		frame.getContentPane().add(txtClients);
		txtClients.setColumns(10);

		txtQueues = new JTextField();
		txtQueues.setText("Queues");
		txtQueues.setHorizontalAlignment(SwingConstants.CENTER);
		txtQueues.setEditable(false);
		txtQueues.setBounds(61, 250, 116, 22);
		frame.getContentPane().add(txtQueues);
		txtQueues.setColumns(10);

		txtSimulationTime = new JTextField();
		txtSimulationTime.setHorizontalAlignment(SwingConstants.CENTER);
		txtSimulationTime.setText("Simulation Time");
		txtSimulationTime.setEditable(false);
		txtSimulationTime.setBounds(61, 349, 116, 22);
		frame.getContentPane().add(txtSimulationTime);
		txtSimulationTime.setColumns(10);

		txtArrivalTime = new JTextField();
		txtArrivalTime.setText("Arrival TIme");
		txtArrivalTime.setHorizontalAlignment(SwingConstants.CENTER);
		txtArrivalTime.setEditable(false);
		txtArrivalTime.setBounds(61, 456, 116, 22);
		frame.getContentPane().add(txtArrivalTime);
		txtArrivalTime.setColumns(10);

		txtServiceTime = new JTextField();
		txtServiceTime.setHorizontalAlignment(SwingConstants.CENTER);
		txtServiceTime.setText("Service Time");
		txtServiceTime.setEditable(false);
		txtServiceTime.setBounds(61, 550, 116, 22);
		frame.getContentPane().add(txtServiceTime);
		txtServiceTime.setColumns(10);
	}
}
