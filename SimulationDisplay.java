package frontEnd;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.Controller;

public class SimulationDisplay extends Thread {

	private final int ONE_TIME_UNIT = 1000;

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SimulationDisplay window = new SimulationDisplay(5, null);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JTextArea display = new JTextArea();
	private int nrQueues;
	private Controller ctr;
	private JScrollPane scroll;

	/**
	 * Create the application.
	 */
	public SimulationDisplay(int nrQueues, Controller ctr) {
		this.nrQueues = nrQueues;
		this.ctr = ctr;
		initialize(nrQueues);
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(int nrQueues) {

		frame = new JFrame();
		frame.setBounds(100, 100, 900, 900);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout());
		display.setRows(50);
		display.setColumns(50);

		display.getAutoscrolls();
		display.setBackground(Color.white);
		display.setEditable(false);
		display.setBounds(0, 0, 900, 900);
		frame.getContentPane().add(display);

		display.setLineWrap(true);
		scroll = new JScrollPane(display);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		frame.getContentPane().add(scroll);

		start();
	}

	@Override
	public void run() {

		while (true) {

			try {
				sleep(this.ONE_TIME_UNIT);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			display.append(ctr.info.toString());
		}

	}

}
