package data;

public class Client {
	
	private int clientID = 0;
	private int arrivalTime = 0;
	private int serviceTime = 0;
	private final int SERVICE_TIME;
	public int queueItIsAt = -1;
	
	private int waitingTime = 0;

	public Client(int clientID, int arrivalTime, int serviceTime) {
		
		this.clientID = clientID;
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
		this.SERVICE_TIME = serviceTime;
	}
	
	public void timeUnitPassed() {
		waitingTime++;
	}
	
	public int getWaitingTime() {
		return this.waitingTime;
	}
	
	public int getServiceTime() {
		return this.serviceTime;
	}
	
	public int getOriginalServiceTime() {
		return this.SERVICE_TIME;
	}
	
	/*
	 * returns true if the client ended the service time, false otherwise 
	 */
	public boolean decrementServiceTime() {
		
		// decrement waiting time, since it will be incremented in the clients iteration -> the client in front of the line is not waiting
		waitingTime--;
		
		serviceTime--;
		
		return (serviceTime == 0);
	}
	
	public int getArrivalTime() {
		return this.arrivalTime;
	}
	
	public int getClientID() {
		return this.clientID;
	}

}
